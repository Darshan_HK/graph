import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fl_chart/fl_chart.dart';

class HomePage extends StatefulWidget {
  final Widget child;

  HomePage({Key key, this.child}) : super(key: key);

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<charts.Series<Bar, String>> _seriesData;
  List<charts.Series<Death, String>> _seriesPieData;
  final List<Color> gradientColors = [
    Colors.red,
    Colors.redAccent
  ];


  _generateData() {
    var data1 = [
      new Bar('Male', '<20', 2, Colors.blueAccent),
      new Bar('Male', '20-50', 31,Colors.blueAccent),
      new Bar('Male', '>50', 52,Colors.blueAccent),
    ];
    var data2 = [
      new Bar('Female', '<20', 2,Colors.lightBlueAccent),
      new Bar('Female', '20-50', 22,Colors.lightBlueAccent),
      new Bar('Female', '>50', 48,Colors.lightBlueAccent),
    ];


    var piedata = [
      new Death('Accidents', 4, Color(0xff3366cc)),
      new Death('HIV/AIDS', 4, Color(0xff990099)),
      new Death('Heart Disease', 41, Color(0xff109618)),
      new Death('Cancer', 29, Color(0xfffdbe19)),
      new Death('Other', 27, Color(0xffdc3912)),
    ];



    _seriesData.add(
      charts.Series(
        domainFn: (Bar bars, _) => bars.age,
        measureFn: (Bar bars, _) => bars.quantity,
        id: 'Male',
        data: data1,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Bar bars, _) =>
            charts.ColorUtil.fromDartColor(bars._color),
      ),
    );

    _seriesData.add(
      charts.Series(
        domainFn: (Bar bars, _) => bars.age,
        measureFn: (Bar bars, _) => bars.quantity,
        id: 'Female',
        data: data2,
        fillPatternFn: (_, __) => charts.FillPatternType.solid,
        fillColorFn: (Bar bars, _) =>
            charts.ColorUtil.fromDartColor(bars._color),
      ),
    );


    _seriesPieData.add(
      charts.Series(
        domainFn: (Death deaths, _) => deaths.death,
        measureFn: (Death deaths, _) => deaths.deathvalue,
        colorFn: (Death deaths, _) =>
            charts.ColorUtil.fromDartColor(deaths.colorval),
        id: 'Air Pollution',
        data: piedata,
        labelAccessorFn: (Death row, _) => '${row.deathvalue}',
      ),
    );



  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _seriesData = List<charts.Series<Bar, String>>();
    _seriesPieData = List<charts.Series<Death, String>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xff1976d2),
            //backgroundColor: Color(0xff308e1c),
            bottom: TabBar(
              indicatorColor: Color(0xff9962D0),
              tabs: [
                Tab(icon: Icon(FontAwesomeIcons.chartLine)),
                Tab(
                  icon: Icon(FontAwesomeIcons.solidChartBar),
                ),
                Tab(icon: Icon(FontAwesomeIcons.chartPie)),

              ],
            ),
            title: Text('Graphs'),
            centerTitle: true,
          ),
          body: TabBarView(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.all(15)),
                        ListTile(
                          leading: Image.asset("asset/heart.png") ,
                          title: Text("Heart Rate",style: TextStyle(
                              fontSize: 30

                          ),),
                          subtitle: Text("Oct 26 - Nov 1"),
                          trailing: Text("120 bpm",style: TextStyle(
                              fontSize: 20
                          ),),
                        ),
                        Padding(padding: EdgeInsets.all(20)),
                        Container(
                          height: 400,
                          width: 400,
                          child: LineChart(
                            LineChartData(
                              minX: 0,
                              maxX: 6,
                              minY: 0,
                              maxY: 4,
                              titlesData: LineTitles.getTitleData(),
                              gridData: FlGridData(
                                show: true,
                                getDrawingHorizontalLine: (value) {
                                  return FlLine(
                                    color: Colors.black,
                                    strokeWidth: 1,
                                  );
                                },
                                drawVerticalLine: false,
                                getDrawingVerticalLine: (value) {
                                  return FlLine(
                                    color: Colors.black,
                                    strokeWidth: 1,
                                  );
                                },
                              ),
                              borderData: FlBorderData(
                                  show: true,
                                  border: Border.all(color: Colors.black38, width: 2)
                              ),
                              lineBarsData: [
                                LineChartBarData(
                                  spots: [
                                    FlSpot(0, 2),
                                    FlSpot(0.6, 1.4),
                                    FlSpot(1, 1.7),
                                    FlSpot(1.7, 1.5),
                                    FlSpot(2.5, 1.6),
                                    FlSpot(2.8, 1.4),
                                    FlSpot(3.2, 1.7),
                                    FlSpot(4,1.2),
                                    FlSpot(4.7,2),
                                    FlSpot(5.5,2.3)
                                  ],
                                  isCurved: true,
                                  colors: gradientColors,
                                  barWidth: 5,
                                  dotData: FlDotData(show: false),
                                ),
                              ],
                            ),),)
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(top: 10)),
                        Text(
                          'Age/Gender Heart disease patients',style: TextStyle(fontSize: 22.0,fontWeight: FontWeight.bold),),
                        Padding(padding: EdgeInsets.only(top: 10)),
                        Expanded(
                          child: charts.BarChart(
                            _seriesData,
                            animate: false,
                            barGroupingType: charts.BarGroupingType.grouped,
                            behaviors: [new charts.SeriesLegend()],
                            animationDuration: Duration(seconds: 5),

                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Padding(padding: EdgeInsets.only(top: 15)),
                        Text(
                          'Death Causes',style: TextStyle(fontSize: 24.0,fontWeight: FontWeight.bold),),
                        Padding(padding: EdgeInsets.all(10)),
                        SizedBox(height: 20.0,),
                        Expanded(
                          child: charts.PieChart(
                              _seriesPieData,
                              animate: true,
                              animationDuration: Duration(seconds: 5),
                              behaviors: [
                                new charts.DatumLegend(
                                  outsideJustification: charts.OutsideJustification.endDrawArea,
                                  horizontalFirst: false,
                                  desiredMaxRows: 5,
                                  cellPadding: new EdgeInsets.only(right: 4.0, bottom: 4.0),
                                  entryTextStyle: charts.TextStyleSpec(
                                      color: charts.MaterialPalette.purple.shadeDefault,
                                      fontFamily: 'Georgia',
                                      fontSize: 11),
                                )
                              ],
                              defaultRenderer: new charts.ArcRendererConfig(
                                  arcWidth: 100,
                                  arcRendererDecorators: [
                                    new charts.ArcLabelDecorator(
                                        labelPosition: charts.ArcLabelPosition.inside)
                                  ])),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Bar {
  String gender;
  String age;
  int quantity;
  Color _color;

  Bar(this.gender, this.age, this.quantity,this._color);
}

class Death {
  String death;
  double deathvalue;
  Color colorval;

  Death(this.death, this.deathvalue, this.colorval);
}



class LineTitles {
  static getTitleData() => FlTitlesData(
    show: true,
    bottomTitles: SideTitles(
      showTitles: true,
      reservedSize: 40,
      getTextStyles: (value) => const TextStyle(
        color: Colors.black38,
        fontWeight: FontWeight.bold,
        fontSize: 16,
      ),
      getTitles: (value) {
        switch (value.toInt()) {
          case 0:
            return '26';
          case 1:
            return '27';
          case 2:
            return '28';
          case 3:
            return '29';
          case 4:
            return '30';
          case 5:
            return '31';
          case 6:
            return '01';
        }
        return '';
      },
      margin: 12,
    ),
    rightTitles: SideTitles(
      showTitles: true,
      getTextStyles: (value) => const TextStyle(
        color: Colors.black38,
        fontWeight: FontWeight.bold,
        fontSize: 15,
      ),
      getTitles: (value) {
        switch (value.toInt()) {
          case 0:
            return '60';
          case 1:
            return '80';
          case 2:
            return '100';
          case 3:
            return '180';
        }
        return '';
      },
      reservedSize: 35,
      margin: 12,
    ),
  );
}
